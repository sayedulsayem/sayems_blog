<?php
include_once 'inc/header.php';
include_once 'inc/slider.php'
?>

<?php
include_once "config/config.php";
include_once "lib/Database.php";
include_once "helpers/format.php"
?>

<?php
$db= new Database();
$format= new format();
?>

<div class="contentsection contemplete clear">
    <div class="maincontent clear">
        <?php
        $query="SELECT * FROM posts LIMIT 4";
        $post=$db->select($query);
        if ($post){
            $result=$post->fetchall();
            foreach ( $result as $item) {
                $data=$item;
                ?>

            <div class="samepost clear">
            <h2><a href="post.php?id=<?php echo $data["id"]; ?>"><?php echo $data["title"]; ?></a></h2>
            <h4><?php echo $format->formatDate($data["created_at"]); ?> By <a href="#"><?php echo $data["author"]; ?></a></h4>
            <a href="#"><img src="<?php echo "admin/upload/".$data['image']; ?>" alt="post image"/></a>
                <?php echo $format->textShorten($data["body"],400); ?>
            <div class="readmore clear">
                <a href="post.php?id=<?php echo $data["id"]; ?>">Read More</a>
            </div>
        </div>

        <?php
            }
        }
        else
        {
            header("Location:404.php");
        }
        ?>
    </div>

    <?php include_once "inc/sidebar.php"; ?>
</div>

<?php include_once "inc/footer.php"; ?>



